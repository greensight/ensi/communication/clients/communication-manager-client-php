<?php
/**
 * StatusesApiTest
 * PHP version 5
 *
 * @category Class
 * @package  Ensi\CommunicationManagerClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Ensi Communication manager
 *
 * Ensi Communication manager
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: mail@greensight.ru
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the endpoint.
 */

namespace Ensi\CommunicationManagerClient;

use \Ensi\CommunicationManagerClient\Configuration;
use \Ensi\CommunicationManagerClient\ApiException;
use \Ensi\CommunicationManagerClient\ObjectSerializer;
use PHPUnit\Framework\TestCase;

/**
 * StatusesApiTest Class Doc Comment
 *
 * @category Class
 * @package  Ensi\CommunicationManagerClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class StatusesApiTest extends TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test case for createStatus
     *
     * Создание объекта типа Status.
     *
     */
    public function testCreateStatus()
    {
    }

    /**
     * Test case for deleteStatus
     *
     * Удаление объекта типа Status.
     *
     */
    public function testDeleteStatus()
    {
    }

    /**
     * Test case for patchStatus
     *
     * Изменение объекта типа типа Status.
     *
     */
    public function testPatchStatus()
    {
    }

    /**
     * Test case for searchStatuses
     *
     * Поиск объектов типа Status.
     *
     */
    public function testSearchStatuses()
    {
    }
}
