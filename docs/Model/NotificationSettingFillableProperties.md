# # NotificationSettingFillableProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | Notification name | [optional] 
**event** | **int** | Event notification ID from NotificationEventEnum | [optional] 
**channels** | **int[]** |  | [optional] 
**theme** | **string** | Notification theme | [optional] 
**text** | **string** | Notification text | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


