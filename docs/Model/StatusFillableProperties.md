# # StatusFillableProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | Имя статуса | [optional] 
**active** | **bool** | Активность | [optional] 
**default** | **bool** | По умолчанию | [optional] 
**channel** | **int[]** | Каналы | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


