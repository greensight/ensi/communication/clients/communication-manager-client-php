# # SendInternalMessageEventData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**text** | **string** | Текст сообщения | [optional] 
**user_id** | **int** | Id юзера | [optional] 
**user_type** | **int** | Тип пользователя | [optional] 
**chat_id** | **int** | Id чата | [optional] 
**files** | **string[]** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


