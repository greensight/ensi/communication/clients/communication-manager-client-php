# # Variable

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Variable from NotificationVariableEnum | 
**title** | **string** | Variable description | 
**items** | [**\Ensi\CommunicationManagerClient\Dto\Variable[]**](Variable.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


