# # Notification

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Notification ID | 
**customer_id** | **int** | Customer ID | 
**event** | **int** | ID of notification event from NotificationEventEnum | 
**channels** | **int[]** |  | 
**theme** | **string** | Notification theme | 
**text** | **string** | Notification text | 
**created_at** | [**\DateTime**](\DateTime.md) | Date of creation | 
**updated_at** | [**\DateTime**](\DateTime.md) | Date of update | 
**is_viewed** | **bool** | Indicates whether the notification has been viewed | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


