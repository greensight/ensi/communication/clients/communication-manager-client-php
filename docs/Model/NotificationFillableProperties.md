# # NotificationFillableProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**is_viewed** | **bool** | Indicates whether the notification has been viewed | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


