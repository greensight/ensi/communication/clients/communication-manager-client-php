# # SearchNotificationSettingsResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**\Ensi\CommunicationManagerClient\Dto\NotificationSetting[]**](NotificationSetting.md) |  | 
**meta** | [**\Ensi\CommunicationManagerClient\Dto\SearchStatusesResponseMeta**](SearchStatusesResponseMeta.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


