# # VariablesDictionary

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**event** | **int** | Event type from NotificationEventEnum | 
**variables** | [**\Ensi\CommunicationManagerClient\Dto\Variable[]**](Variable.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


