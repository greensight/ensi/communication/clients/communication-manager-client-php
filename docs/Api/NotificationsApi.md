# Ensi\CommunicationManagerClient\NotificationsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createNotificationSetting**](NotificationsApi.md#createNotificationSetting) | **POST** /notification-settings | Creating an object of NotificationSetting
[**deleteNotificationSetting**](NotificationsApi.md#deleteNotificationSetting) | **DELETE** /notification-settings/{id} | Deleting an object of NotificationSetting
[**getNotificationSetting**](NotificationsApi.md#getNotificationSetting) | **GET** /notification-settings/{id} | Get the object of NotificationSetting
[**getVariables**](NotificationsApi.md#getVariables) | **GET** /notification-setting-variables | Getting a directory of available variables by event type
[**massPatchNotifications**](NotificationsApi.md#massPatchNotifications) | **POST** /notifications:mass-patch | Mass patching an object of Notification
[**patchNotificationSetting**](NotificationsApi.md#patchNotificationSetting) | **PATCH** /notification-settings/{id} | Patching an object of NotificationSetting
[**searchNotificationSettings**](NotificationsApi.md#searchNotificationSettings) | **POST** /notification-settings:search | Search for objects of NotificationSetting
[**searchNotifications**](NotificationsApi.md#searchNotifications) | **POST** /notifications:search | Search for objects of Notification



## createNotificationSetting

> \Ensi\CommunicationManagerClient\Dto\NotificationSettingResponse createNotificationSetting($create_notification_setting_request)

Creating an object of NotificationSetting

Creating an object of NotificationSetting

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CommunicationManagerClient\Api\NotificationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_notification_setting_request = new \Ensi\CommunicationManagerClient\Dto\CreateNotificationSettingRequest(); // \Ensi\CommunicationManagerClient\Dto\CreateNotificationSettingRequest | 

try {
    $result = $apiInstance->createNotificationSetting($create_notification_setting_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NotificationsApi->createNotificationSetting: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_notification_setting_request** | [**\Ensi\CommunicationManagerClient\Dto\CreateNotificationSettingRequest**](../Model/CreateNotificationSettingRequest.md)|  |

### Return type

[**\Ensi\CommunicationManagerClient\Dto\NotificationSettingResponse**](../Model/NotificationSettingResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteNotificationSetting

> \Ensi\CommunicationManagerClient\Dto\EmptyDataResponse deleteNotificationSetting($id)

Deleting an object of NotificationSetting

Deleting an object of NotificationSetting

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CommunicationManagerClient\Api\NotificationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteNotificationSetting($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NotificationsApi->deleteNotificationSetting: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\CommunicationManagerClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getNotificationSetting

> \Ensi\CommunicationManagerClient\Dto\NotificationSettingResponse getNotificationSetting($id)

Get the object of NotificationSetting

Get the object of NotificationSetting

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CommunicationManagerClient\Api\NotificationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->getNotificationSetting($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NotificationsApi->getNotificationSetting: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\CommunicationManagerClient\Dto\NotificationSettingResponse**](../Model/NotificationSettingResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getVariables

> \Ensi\CommunicationManagerClient\Dto\VariablesResponse getVariables()

Getting a directory of available variables by event type

Getting a directory of available variables by event type

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CommunicationManagerClient\Api\NotificationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getVariables();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NotificationsApi->getVariables: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\Ensi\CommunicationManagerClient\Dto\VariablesResponse**](../Model/VariablesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## massPatchNotifications

> \Ensi\CommunicationManagerClient\Dto\EmptyDataResponse massPatchNotifications($mass_patch_notifications_request)

Mass patching an object of Notification

Mass patching an object of Notification

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CommunicationManagerClient\Api\NotificationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$mass_patch_notifications_request = new \Ensi\CommunicationManagerClient\Dto\MassPatchNotificationsRequest(); // \Ensi\CommunicationManagerClient\Dto\MassPatchNotificationsRequest | 

try {
    $result = $apiInstance->massPatchNotifications($mass_patch_notifications_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NotificationsApi->massPatchNotifications: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mass_patch_notifications_request** | [**\Ensi\CommunicationManagerClient\Dto\MassPatchNotificationsRequest**](../Model/MassPatchNotificationsRequest.md)|  |

### Return type

[**\Ensi\CommunicationManagerClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchNotificationSetting

> \Ensi\CommunicationManagerClient\Dto\NotificationSettingResponse patchNotificationSetting($id, $patch_notification_setting_request)

Patching an object of NotificationSetting

Patching an object of NotificationSetting

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CommunicationManagerClient\Api\NotificationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_notification_setting_request = new \Ensi\CommunicationManagerClient\Dto\PatchNotificationSettingRequest(); // \Ensi\CommunicationManagerClient\Dto\PatchNotificationSettingRequest | 

try {
    $result = $apiInstance->patchNotificationSetting($id, $patch_notification_setting_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NotificationsApi->patchNotificationSetting: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_notification_setting_request** | [**\Ensi\CommunicationManagerClient\Dto\PatchNotificationSettingRequest**](../Model/PatchNotificationSettingRequest.md)|  |

### Return type

[**\Ensi\CommunicationManagerClient\Dto\NotificationSettingResponse**](../Model/NotificationSettingResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchNotificationSettings

> \Ensi\CommunicationManagerClient\Dto\SearchNotificationSettingsResponse searchNotificationSettings($search_notification_settings_request)

Search for objects of NotificationSetting

Search for objects of NotificationSetting

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CommunicationManagerClient\Api\NotificationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_notification_settings_request = new \Ensi\CommunicationManagerClient\Dto\SearchNotificationSettingsRequest(); // \Ensi\CommunicationManagerClient\Dto\SearchNotificationSettingsRequest | 

try {
    $result = $apiInstance->searchNotificationSettings($search_notification_settings_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NotificationsApi->searchNotificationSettings: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_notification_settings_request** | [**\Ensi\CommunicationManagerClient\Dto\SearchNotificationSettingsRequest**](../Model/SearchNotificationSettingsRequest.md)|  |

### Return type

[**\Ensi\CommunicationManagerClient\Dto\SearchNotificationSettingsResponse**](../Model/SearchNotificationSettingsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchNotifications

> \Ensi\CommunicationManagerClient\Dto\SearchNotificationsResponse searchNotifications($search_notifications_request)

Search for objects of Notification

Search for object of Notification

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CommunicationManagerClient\Api\NotificationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_notifications_request = new \Ensi\CommunicationManagerClient\Dto\SearchNotificationsRequest(); // \Ensi\CommunicationManagerClient\Dto\SearchNotificationsRequest | 

try {
    $result = $apiInstance->searchNotifications($search_notifications_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NotificationsApi->searchNotifications: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_notifications_request** | [**\Ensi\CommunicationManagerClient\Dto\SearchNotificationsRequest**](../Model/SearchNotificationsRequest.md)|  |

### Return type

[**\Ensi\CommunicationManagerClient\Dto\SearchNotificationsResponse**](../Model/SearchNotificationsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

