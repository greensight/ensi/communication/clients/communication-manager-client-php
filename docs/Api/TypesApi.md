# Ensi\CommunicationManagerClient\TypesApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createType**](TypesApi.md#createType) | **POST** /types | Создание объекта типа Type
[**deleteType**](TypesApi.md#deleteType) | **DELETE** /types/{id} | Удаление объекта типа Type
[**patchType**](TypesApi.md#patchType) | **PATCH** /types/{id} | Изменение объекта типа Type
[**searchTypes**](TypesApi.md#searchTypes) | **POST** /types:search | Поиск объектов типа Type



## createType

> \Ensi\CommunicationManagerClient\Dto\TypeResponse createType($create_type_request)

Создание объекта типа Type

Создание объекта типа Type

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CommunicationManagerClient\Api\TypesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_type_request = new \Ensi\CommunicationManagerClient\Dto\CreateTypeRequest(); // \Ensi\CommunicationManagerClient\Dto\CreateTypeRequest | 

try {
    $result = $apiInstance->createType($create_type_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TypesApi->createType: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_type_request** | [**\Ensi\CommunicationManagerClient\Dto\CreateTypeRequest**](../Model/CreateTypeRequest.md)|  |

### Return type

[**\Ensi\CommunicationManagerClient\Dto\TypeResponse**](../Model/TypeResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteType

> \Ensi\CommunicationManagerClient\Dto\EmptyDataResponse deleteType($id)

Удаление объекта типа Type

Удаление объекта типа Type

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CommunicationManagerClient\Api\TypesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteType($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TypesApi->deleteType: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\CommunicationManagerClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchType

> \Ensi\CommunicationManagerClient\Dto\TypeResponse patchType($id, $patch_type_request)

Изменение объекта типа Type

Изменение объекта типа Type

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CommunicationManagerClient\Api\TypesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_type_request = new \Ensi\CommunicationManagerClient\Dto\PatchTypeRequest(); // \Ensi\CommunicationManagerClient\Dto\PatchTypeRequest | 

try {
    $result = $apiInstance->patchType($id, $patch_type_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TypesApi->patchType: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_type_request** | [**\Ensi\CommunicationManagerClient\Dto\PatchTypeRequest**](../Model/PatchTypeRequest.md)|  |

### Return type

[**\Ensi\CommunicationManagerClient\Dto\TypeResponse**](../Model/TypeResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchTypes

> \Ensi\CommunicationManagerClient\Dto\SearchTypesResponse searchTypes($search_types_request)

Поиск объектов типа Type

Поиск объектов типа Type

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CommunicationManagerClient\Api\TypesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_types_request = new \Ensi\CommunicationManagerClient\Dto\SearchTypesRequest(); // \Ensi\CommunicationManagerClient\Dto\SearchTypesRequest | 

try {
    $result = $apiInstance->searchTypes($search_types_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TypesApi->searchTypes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_types_request** | [**\Ensi\CommunicationManagerClient\Dto\SearchTypesRequest**](../Model/SearchTypesRequest.md)|  |

### Return type

[**\Ensi\CommunicationManagerClient\Dto\SearchTypesResponse**](../Model/SearchTypesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

