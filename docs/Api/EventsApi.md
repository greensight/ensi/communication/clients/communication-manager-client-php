# Ensi\CommunicationManagerClient\EventsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**parseEvent**](EventsApi.md#parseEvent) | **POST** /event | Отправка события



## parseEvent

> \Ensi\CommunicationManagerClient\Dto\EmptyDataResponse parseEvent($unknown_base_type)

Отправка события

Отправка события

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CommunicationManagerClient\Api\EventsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$unknown_base_type = new \Ensi\CommunicationManagerClient\Dto\UNKNOWN_BASE_TYPE(); // \Ensi\CommunicationManagerClient\Dto\UNKNOWN_BASE_TYPE | 

try {
    $result = $apiInstance->parseEvent($unknown_base_type);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EventsApi->parseEvent: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **unknown_base_type** | [**\Ensi\CommunicationManagerClient\Dto\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  |

### Return type

[**\Ensi\CommunicationManagerClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

