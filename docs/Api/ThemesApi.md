# Ensi\CommunicationManagerClient\ThemesApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createTheme**](ThemesApi.md#createTheme) | **POST** /themes | Создание объекта типа Theme
[**deleteTheme**](ThemesApi.md#deleteTheme) | **DELETE** /themes/{id} | Удаление объекта типа Theme
[**patchTheme**](ThemesApi.md#patchTheme) | **PATCH** /themes/{id} | Изменение объекта типа типа Theme
[**searchThemes**](ThemesApi.md#searchThemes) | **POST** /themes:search | Поиск объектов типа Theme



## createTheme

> \Ensi\CommunicationManagerClient\Dto\ThemeResponse createTheme($create_theme_request)

Создание объекта типа Theme

Создание объекта типа Theme

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CommunicationManagerClient\Api\ThemesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_theme_request = new \Ensi\CommunicationManagerClient\Dto\CreateThemeRequest(); // \Ensi\CommunicationManagerClient\Dto\CreateThemeRequest | 

try {
    $result = $apiInstance->createTheme($create_theme_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ThemesApi->createTheme: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_theme_request** | [**\Ensi\CommunicationManagerClient\Dto\CreateThemeRequest**](../Model/CreateThemeRequest.md)|  |

### Return type

[**\Ensi\CommunicationManagerClient\Dto\ThemeResponse**](../Model/ThemeResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteTheme

> \Ensi\CommunicationManagerClient\Dto\EmptyDataResponse deleteTheme($id)

Удаление объекта типа Theme

Удаление объекта типа Theme

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CommunicationManagerClient\Api\ThemesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteTheme($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ThemesApi->deleteTheme: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\CommunicationManagerClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchTheme

> \Ensi\CommunicationManagerClient\Dto\ThemeResponse patchTheme($id, $patch_theme_request)

Изменение объекта типа типа Theme

Изменение объекта типа типа Theme

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CommunicationManagerClient\Api\ThemesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_theme_request = new \Ensi\CommunicationManagerClient\Dto\PatchThemeRequest(); // \Ensi\CommunicationManagerClient\Dto\PatchThemeRequest | 

try {
    $result = $apiInstance->patchTheme($id, $patch_theme_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ThemesApi->patchTheme: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_theme_request** | [**\Ensi\CommunicationManagerClient\Dto\PatchThemeRequest**](../Model/PatchThemeRequest.md)|  |

### Return type

[**\Ensi\CommunicationManagerClient\Dto\ThemeResponse**](../Model/ThemeResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchThemes

> \Ensi\CommunicationManagerClient\Dto\SearchThemesResponse searchThemes($search_themes_request)

Поиск объектов типа Theme

Поиск объектов типа Theme

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CommunicationManagerClient\Api\ThemesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_themes_request = new \Ensi\CommunicationManagerClient\Dto\SearchThemesRequest(); // \Ensi\CommunicationManagerClient\Dto\SearchThemesRequest | 

try {
    $result = $apiInstance->searchThemes($search_themes_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ThemesApi->searchThemes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_themes_request** | [**\Ensi\CommunicationManagerClient\Dto\SearchThemesRequest**](../Model/SearchThemesRequest.md)|  |

### Return type

[**\Ensi\CommunicationManagerClient\Dto\SearchThemesResponse**](../Model/SearchThemesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

