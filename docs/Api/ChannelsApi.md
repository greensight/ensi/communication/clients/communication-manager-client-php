# Ensi\CommunicationManagerClient\ChannelsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createChannel**](ChannelsApi.md#createChannel) | **POST** /channels | Создание объекта типа Channel
[**deleteChannel**](ChannelsApi.md#deleteChannel) | **DELETE** /channels/{id} | Удаление объекта типа Channel
[**patchChannel**](ChannelsApi.md#patchChannel) | **PATCH** /channels/{id} | Изменение объекта типа Channel
[**searchChannels**](ChannelsApi.md#searchChannels) | **POST** /channels:search | Поиск объектов типа Channel



## createChannel

> \Ensi\CommunicationManagerClient\Dto\ChannelResponse createChannel($create_channel_request)

Создание объекта типа Channel

Создание объекта типа Channel

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CommunicationManagerClient\Api\ChannelsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_channel_request = new \Ensi\CommunicationManagerClient\Dto\CreateChannelRequest(); // \Ensi\CommunicationManagerClient\Dto\CreateChannelRequest | 

try {
    $result = $apiInstance->createChannel($create_channel_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ChannelsApi->createChannel: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_channel_request** | [**\Ensi\CommunicationManagerClient\Dto\CreateChannelRequest**](../Model/CreateChannelRequest.md)|  |

### Return type

[**\Ensi\CommunicationManagerClient\Dto\ChannelResponse**](../Model/ChannelResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteChannel

> \Ensi\CommunicationManagerClient\Dto\EmptyDataResponse deleteChannel($id)

Удаление объекта типа Channel

Удаление объекта типа Channel

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CommunicationManagerClient\Api\ChannelsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteChannel($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ChannelsApi->deleteChannel: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\CommunicationManagerClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchChannel

> \Ensi\CommunicationManagerClient\Dto\ChannelResponse patchChannel($id, $patch_channel_request)

Изменение объекта типа Channel

Изменение объекта типа Channel

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CommunicationManagerClient\Api\ChannelsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_channel_request = new \Ensi\CommunicationManagerClient\Dto\PatchChannelRequest(); // \Ensi\CommunicationManagerClient\Dto\PatchChannelRequest | 

try {
    $result = $apiInstance->patchChannel($id, $patch_channel_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ChannelsApi->patchChannel: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_channel_request** | [**\Ensi\CommunicationManagerClient\Dto\PatchChannelRequest**](../Model/PatchChannelRequest.md)|  |

### Return type

[**\Ensi\CommunicationManagerClient\Dto\ChannelResponse**](../Model/ChannelResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchChannels

> \Ensi\CommunicationManagerClient\Dto\SearchChannelsResponse searchChannels($search_channels_request)

Поиск объектов типа Channel

Поиск объектов типа Channel

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\CommunicationManagerClient\Api\ChannelsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_channels_request = new \Ensi\CommunicationManagerClient\Dto\SearchChannelsRequest(); // \Ensi\CommunicationManagerClient\Dto\SearchChannelsRequest | 

try {
    $result = $apiInstance->searchChannels($search_channels_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ChannelsApi->searchChannels: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_channels_request** | [**\Ensi\CommunicationManagerClient\Dto\SearchChannelsRequest**](../Model/SearchChannelsRequest.md)|  |

### Return type

[**\Ensi\CommunicationManagerClient\Dto\SearchChannelsResponse**](../Model/SearchChannelsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

